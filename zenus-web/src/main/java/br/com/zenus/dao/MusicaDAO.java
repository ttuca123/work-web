package br.com.zenus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.zenus.model.Musica;

public class MusicaDAO {

	private EntityManager em;

	private MusicaDAO() {

	}

	public MusicaDAO(EntityManager entityManager) {

		this.em = entityManager;
	}

	public void salvar(Musica musica) throws Exception {
		if (musica.getId() == null) {
			this.em.persist(musica);
		} else {
			if (em.find(Musica.class, musica.getId()) == null) {
				throw new Exception("Instru��o SQL nao existe!");
			}
			em.merge(musica);
		}
	}

	public void excluir(Long id) {
		Musica btpInstruacaoSql = em.find(Musica.class, id);
		em.remove(btpInstruacaoSql);
	}

	public Musica consultarPorId(Long id) {

		Musica musica = null;

		try {
			musica = em.find(Musica.class, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return musica;

	}

	@SuppressWarnings("unchecked")
	public List<Musica> listar() {

		List<Musica> musicas = (List<Musica>) em.createQuery("select m from Musica m").getResultList();

		return musicas;
	}

}
