package br.com.zenus.service;

public interface GenericService {

	public void salvar(Object object);

	public Object buscar(Long id);
	
	
	public void deletar(Long id);
	


}
