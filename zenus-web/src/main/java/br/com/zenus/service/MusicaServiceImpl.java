package br.com.zenus.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.zenus.dao.MusicaDAO;
import br.com.zenus.model.Musica;

@Stateless(name = "MusicaService")
public class MusicaServiceImpl implements MusicaService {

	@PersistenceContext(name = "zenus-web")
	private EntityManager em;

	private MusicaDAO musicaDAO;

	public void salvar(Object object) {

		musicaDAO = new MusicaDAO(em);

		try {
			musicaDAO.salvar((Musica) object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Musica> getMusicas() {

		musicaDAO = new MusicaDAO(em);

		return musicaDAO.listar();

	}

	public Object buscar(Long id) {

		musicaDAO = new MusicaDAO(em);

		return musicaDAO.consultarPorId(id);

	}

	public void deletar(Long id) {

		musicaDAO = new MusicaDAO(em);

		musicaDAO.excluir(id);
	}

}
