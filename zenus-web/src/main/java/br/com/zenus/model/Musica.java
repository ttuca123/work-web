package br.com.zenus.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Musica
 *
 */
@Entity
@Table(name = "musica")
public class Musica implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	public Long getId() {
		return id;
	}

	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String artista;	
	

	@Column(nullable = false)
	private int tom;

	@Column
	private String observacao;

	
	
	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public Musica() {
		super();
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getTom() {
		return this.tom;
	}

	public void setTom(int tom) {
		this.tom = tom;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
