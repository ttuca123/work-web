package br.com.zenus.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "repertorio")
public class Repertorio implements Serializable {

	private static final long serialVersionUID = 1L;

	public Repertorio() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "seq_repertorio")
	private Long seqRepertorio;

	@OneToOne(cascade = CascadeType.ALL)
	private Banda banda;

	@ManyToMany(cascade = CascadeType.ALL, fetch= FetchType.LAZY)
	@JoinColumn(name = "repertorio_musica")
	private Collection<Musica> musicas;

	public Collection<Musica> getMusicas() {
		return musicas;
	}

	public void setMusicas(Collection<Musica> musicas) {
		this.musicas = musicas;
	}

	public Banda getBanda() {
		return banda;
	}

	public void setBanda(Banda banda) {
		this.banda = banda;
	}

	public Long getSeqRepertorio() {
		return seqRepertorio;
	}
}
