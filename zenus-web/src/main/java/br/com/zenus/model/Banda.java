package br.com.zenus.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Musico
 *
 */
@Entity
@Table(name = "banda")

public class Banda implements Serializable {

	private static final long serialVersionUID = 1L;

	public Banda() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "seq_banda")
	private Long seqBanda;

	@Column(nullable = false, unique = true)
	private String nome;

	@Column(name = "fone")
	private Long telefone;

	@Column(name = "email")
	private String email;

	@ManyToOne
	private Ensaio ensaio;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "banda_repertorio")
	private List<Repertorio> repertorios;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "banda_musico")
	private Collection<Musico> musico;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Ensaio getEnsaio() {
		return ensaio;
	}

	public void setEnsaio(Ensaio ensaio) {
		this.ensaio = ensaio;
	}

	public List<Repertorio> getRepertorios() {
		return repertorios;
	}

	public void setRepertorios(List<Repertorio> repertorios) {
		this.repertorios = repertorios;
	}

	public Long getTelefone() {
		return telefone;
	}

	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}

	public String getInstrumento() {
		return email;
	}

	public void setInstrumento(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return seqBanda;
	}

}
