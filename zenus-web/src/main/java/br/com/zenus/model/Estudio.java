package br.com.zenus.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "estudio")
public class Estudio implements Serializable {

	private static final long serialVersionUID = 1L;

	public Estudio() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "seq_estudio")
	private Long seqEstudio;

	public Long getSeqRepertorio() {
		return seqEstudio;
	}

	@Column(name = "nome")
	private String nome;

	@Column(name = "responsavel")
	private String responsavel;

	@Column(name = "telefone")
	private Long telefone;

	@Column(name = "endereco")
	private String endereco;

	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public Long getTelefone() {
		return telefone;
	}

	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Long getSeqEstudio() {
		return seqEstudio;
	}

}