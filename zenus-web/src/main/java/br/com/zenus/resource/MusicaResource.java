package br.com.zenus.resource;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.zenus.model.Musica;
import br.com.zenus.service.MusicaService;

@Path("/musicas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MusicaResource {

	@EJB
	MusicaService musicaService;

	@POST
	public void salvar(Musica musica) {
		musicaService.salvar(musica);
	}

	@POST
	@Path("/{id}")
	public void deletarMusica(@PathParam("id") Long id) {

		musicaService.deletar(id);

	}

	@GET
	@Path("/{id}")
	public Response getMusica(@PathParam("id") Long id) {

		Musica musica = (Musica) musicaService.buscar(id);

		if (musica == null) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(musica).build();

	}

	@GET
	public Response getMusicas() {

		List<Musica> musicas = musicaService.getMusicas();

		if (musicas == null || musicas.isEmpty()) {

			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(musicas).build();

	}

}
